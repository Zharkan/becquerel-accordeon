{
  const { registerBlockType } = wp.blocks;
  const { createElement, Component } = wp.element;
  const { __ } = wp.i18n;
  const {
    InspectorControls,
    PanelColorSettings,
    RichText,
    BlockControls,
    InnerBlocks,
  } = wp.blockEditor;
  const {
    ToggleControl,
    SelectControl,
    PanelBody,
  } = wp.components;

  registerBlockType("becquerel-accordeon/becquerel-accordeon", {
    title: __("Becquerel Accordéon"),
    category: __("widgets"),
    icon: "editor-ul",
    keywords: [__("accordeon"), __("list"), __("collapse"), __("collapsable")],
    supports: {
      anchor: true,
    },
    attributes: {
      content: {
        type: "array",
        source: "children",
      },
      anchor: {
        type: "string",
        default: null,
      },
      title: {
        type: "string",
        default: null,
      },
      title_tag: {
        type: "string",
        default: "span",
      },
      title_text_color: {
        type: "string",
        default: false,
      },
      title_background_color: {
        type: "string",
        default: false,
      },
      border_color: {
        type: "string",
        default: false,
      },
      accordeon_open: {
        type: "boolean",
        default: false,
      },
      schema: {
        type: "string",
        default: false,
      },
    },

    example: {
      attributes: {
        title: __("Titre"),
        bordered: true,
      },
    },

    edit: function (props) {
      const attributes = props.attributes;
      const setAttributes = props.setAttributes;
      var content = props.attributes.content;
      function changeContent(newContent) {
        setAttributes({ content: newContent });
      }
      function changeTitle(title) {
        setAttributes({ title });
      }
      function changeTitleTag(title_tag) {
        setAttributes({ title_tag });
      }
      function changeTitleTextColor(title_text_color) {
        setAttributes({ title_text_color });
      }
      function changeTitleBackgroundColor(title_background_color) {
        setAttributes({ title_background_color });
      }
      function changeBorderColor(border_color) {
        setAttributes({ border_color });
      }
      function changeSchema(schema) {
        setAttributes({ schema });
      }
      function changeaccordeonOpen(accordeon_open) {
        setAttributes({ accordeon_open });
      }

      return createElement(
        "div",
        {
          className: "becquerel-accordeon " + attributes.borderedClass,
          id: attributes.anchor,
          style: {
            color: attributes.title_text_color,
            background: attributes.title_background_color,
            borderLeft: attributes.border_color,
          },
        },
        [
          createElement(BlockControls, {
            key: "controls",
          }),
          createElement(
            "summary",
            {
              className: "becquerel-accordeon-title " + props.className,
              style: {
                color: attributes.title_text_color,
                background: attributes.title_background_color,
                borderLeft: attributes.border_color,
              },
            },
            createElement(RichText, {
              tagName: "span",
              role: "textbox",
              onChange: changeTitle,
              value: attributes.title,
              keepPlaceholderOnFocus: true,
              placeholder: __("Titre de l'accordéon..."),
              allowedFormats: ["core/bold", "core/italic"],
            })
          ),

          createElement(
            "div",
            {
              className: "becquerel-accordeon-body " + props.className,
              style: {
                borderColor: attributes.border_color,
              },
            },
            createElement(InnerBlocks)
          ),

          //Block inspector
          createElement(
            InspectorControls,
            {},
            createElement(PanelBody, {}, [
              createElement(ToggleControl, {
                checked: attributes.accordeon_open,
                label: __("Ouvert par défaut ?"),
                onChange: changeaccordeonOpen,
              }),
              /*createElement(ToggleControl, {
							checked: attributes.bordered,
							label: __( 'Border?' ),
							onChange: changeBordered
						}),*/
              createElement(SelectControl, {
                value: attributes.schema,
                label: __("Markup SEO (si FAQ) :"),
                onChange: changeSchema,
                options: [
                  { value: false, label: "None" },
                  { value: "faq", label: "FAQ" },
                ],
              }),
            ]),
            createElement(SelectControl, {
              value: attributes.title_tag,
              label: __("TAGS HTML pour l'accordéon"),
              onChange: changeTitleTag,
              options: [
                { value: "span", label: "span" },
                { value: "div", label: "div" },
                { value: "p", label: "p" },
                { value: "h1", label: "H1" },
                { value: "h2", label: "H2" },
                { value: "h3", label: "H3" },
                { value: "h4", label: "H4" },
              ],
            }),

            createElement(PanelColorSettings, {
              title: __("Sélection des couleurs"),
              colorSettings: [
                {
                  value: attributes.title_text_color,
                  onChange: changeTitleTextColor,
                  label: __("Couleur du titre"),
                  colors: [
                    {
                      name: "white",
                      color: "#fff",
                    },
                    {
                      name: "black",
                      color: "#222",
                    },
                  ],
                },
                {
                  value: attributes.title_background_color,
                  onChange: changeTitleBackgroundColor,
                  label: __("Couleur d'arrière plan du titre"),
                  colors: [
                    {
                      name: "Saumon",
                      color: "#FFEFC4",
                    },
                    {
                      name: "Bleu",
                      color: "#E7F7F4",
                    },
                    { name: "Saumon 2", color: "#FAE2D6" },
                  ],
                },
                {
                  value: attributes.border_color,
                  onChange: changeBorderColor,
                  label: __("Couleur de la bordure"),
                  colors: [
                    {
                      name: "Saumon",
                      color: "#FFEFC4",
                    },
                    {
                      name: "Bleu",
                      color: "#00A083",
                    },
                    { name: "Bleu-Medium", color: "#0072BB" },
                    { name: "Orange-Medium", color: "#EA5711" },
                  ],
                },
              ],
            })
          ),
        ]
      );
    },
    save: function (props) {
      return createElement(InnerBlocks.Content, {
        className: props.className,
        value: props.attributes.content,
      });
    },
  });
}
