<?php

/**
 * Plugin Name: Becquerel Accordéon
 * Plugin URI: https://www.gaylordjulien.dev/
 * Description: Block gutenberg pour créer et gérer des accordéons, s'adapte aux couleurs du site internet.
 * Version: 1.5.7
 * Text Domain: becquerel-accordéon
 * Author: Gaylord Julien
 * Author URI: https://www.gaylordjulien.dev/
 */

//Enqueue CSS when in use
add_filter('the_content', 'enqueue_becquerel_accordeon_styles');
add_action('enqueue_block_editor_assets', 'enqueue_becquerel_accordeon_styles');
function enqueue_becquerel_accordeon_styles($content = "")
{
    global $post;
    $include_frontend_stylesheet = apply_filters('becquerel_accordeon_include_frontend_stylesheet', true);
    $include_admin_stylesheet = apply_filters('becquerel_accordeon_include_admin_stylesheet', true);

    $plugin_url = plugin_dir_url(__FILE__);

    if ($include_frontend_stylesheet && (has_shortcode($post->post_content, 'becquerel-accordeon') || has_block('becquerel-accordeon/becquerel-accordeon'))) {
        wp_enqueue_style('becquerel-accordeon', $plugin_url . 'becquerel-accordeon.css', array(), '1.3.2');
        wp_enqueue_script('script', plugins_url('/js/script.js', __FILE__));
    }

    if ($include_admin_stylesheet && (is_admin())) {
        wp_enqueue_style('becquerel-accordeon-admin-styles', $plugin_url . 'editor-styles.css', array(), '1.3.2');
    }

    return $content;
}

//Shortcode function to display becquerel accordeon
function becquerel_accordeon_shortcode($atts, $content = null)
{
    $atts = shortcode_atts(array(
        'anchor' => null,
        'title' => null,
        'title_tag' => 'span',
        'accordeon_open' => false,
        'bordered' => false,
        'title_background_color' => false,
        'border_color' => false,
        'title_text_color' => false,
        'schema' => false,
        'class' => false
    ), $atts, 'becquerel-accordeon');

    return render_becquerel_accordeon($atts, $content, false);
}
add_shortcode('becquerel-accordeon', 'becquerel_accordeon_shortcode');

//Block handler for Gutenberg
function becquerel_accordeon_block_handler($atts, $content)
{
    return render_becquerel_accordeon($atts, $content, true);
}

//Render the actual accordeon
function render_becquerel_accordeon($options, $content, $isBlock)
{
    $output = '';

    $process_shortcodes = apply_filters('becquerel_accordeon_process_shortcodes', true);

    if ($process_shortcodes) {
        $content = do_shortcode($content);
    }

    if (!$isBlock) {
        $content = wpautop(preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', force_balance_tags($content)));
    }

    $anchor = '';
    if (isset($options['anchor']) && $options['anchor']) {
        $anchor = ' id="' . $options['anchor'] . '"';
    }

    $open = '';
    if ($options['accordeon_open']) {
        $open = ' open';
    }

    $classes = array(
        'becquerel-accordeon'
    );
    if ($options['bordered']) {
        $classes[] = 'bordered';
    }
    if (isset($options['class']) && $options['class']) {
        $classes[] = $options['class'];
    }
    if (isset($options['className']) && $options['className']) {
        $classes[] = $options['className'];
    }

    $bodyClasses = array(
        'becquerel-accordeon-body'
    );

    $titleStyles = $bodyStyles = array();
    if ($options['title_text_color']) {
        $titleStyles[] = 'color:' . $options['title_text_color'];
        $classes[] = 'has-text-color';
    }
    if ($options['title_background_color']) {
        $titleStyles[] = 'background:' . $options['title_background_color'];
        $bodyStyles[] = 'border-left: 6px solid ' . $options['border_color'];
        $classes[] = 'has-background';
    }
    if (!empty($titleStyles)) {
        $titleStyles = ' style="' . implode(';', $titleStyles) . ';"';
    } else {
        $titleStyles = '';
    }
    if (!empty($bodyStyles)) {
        $bodyStyles = ' style="' . implode(';', $bodyStyles) . ';"';
    } else {
        $bodyStyles = '';
    }

    $propBox = $propTitle = $propContent = null;
    if (isset($options['schema']) && $options['schema'] == 'faq') {
        global $becquerel_accordeon_schema;
        if (!isset($becquerel_accordeon_schema) || !is_array($becquerel_accordeon_schema)) {
            $becquerel_accordeon_schema = array(
                '@context' => "https://schema.org",
                '@type' => 'FAQPage',
                'mainEntity' => array()
            );
        }
        $becquerel_accordeon_schema['mainEntity'][] = array(
            '@type' => 'Question',
            'name' => $options['title'],
            'acceptedAnswer' => array(
                '@type' => 'Answer',
                'text' => $content
            )
        );

        $output_microdata = apply_filters('becquerel_accordeon_output_microdata', false);
        if ($output_microdata) {
            $propBox = ' itemscope itemprop="mainEntity" itemtype="https://schema.org/Question"';
            $propTitle = ' itemprop="name"';
            $content = ' <span itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"><span itemprop="text">' . $content . '</span></span>';
            $becquerel_accordeon_schema = null;
        }
    }

    if ($options['title'] && isset($content)) {
        $output .= '<div class="' . implode(' ', $classes) . '"' . $anchor . ' ' . $bodyStyles . '><details' . $propBox . ' '. $open .'><summary  class="becquerel-accordeon-title"' . $titleStyles . '><' . $options['title_tag'] . '' . $propTitle . '>' . $options['title'] . '</' . $options['title_tag'] . ' ><div class="summary-chevron-up">
			
			
			<svg ' . $titleStyles . ' width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg" class="feather feather-chevron-down"><path stroke="currentColor" style="stroke-width:1.27645" d="M11.857 21.018V2.783M1.867 11.496h20.105"/></svg>
		</div></summary><div class="' . implode(' ', $bodyClasses) . '" ' . $titleStyles .'>';
        $output .= $content;
        $output .= '</div><div class="summary-chevron-down">
		<svg ' . $titleStyles . '  width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><path stroke="currentColor" style="stroke-width:1.32469" d="m5.181 4.738 14.716 14.786"/><path stroke="currentColor" style="stroke-width:1.30048" d="M19.738 4.502 6.12 19.757"/></svg>
		</details></div>';
    }

    return $output;
}

//Output JSON-LD schema in the footer
add_action('wp_footer', 'becquerel_accordeon_output_schema');
function becquerel_accordeon_output_schema()
{
    global $becquerel_accordeon_schema;

    if (is_array($becquerel_accordeon_schema)) {
        $output = '<script type="application/ld+json" class="becquerel-accordeon-faq-json">';
        $output .= wp_json_encode($becquerel_accordeon_schema);
        $output .= '</script>';
        echo $output;
    }
}

//Register Gutenberg block
add_action('init', function () {
    // Skip block registration if Gutenberg is not enabled.
    if (!function_exists('register_block_type')) {
        return;
    }
    $dir = dirname(__FILE__);

    $index_js = 'block/index.js';
    wp_register_script('becquerel-accordeon', plugins_url($index_js, __FILE__), array(
        'wp-editor',
        'wp-blocks',
        'wp-i18n',
        'wp-element',
        'wp-components'
    ), filemtime("$dir/$index_js"));

    register_block_type('becquerel-accordeon/becquerel-accordeon', array(
        'editor_script' => 'becquerel-accordeon',
        'render_callback' => 'becquerel_accordeon_block_handler',
        'attributes' => [
            'content' => [
                'type' => 'array',
                'default' => 'Content'
            ],
            'title' => [
                'type' => 'string',
                'default' => 'accordeon Title'
            ],
            'accordeon_open' => [
                'type' => 'boolean',
                'default' => false
            ],
            'bordered' => [
                'type' => 'boolean',
                'default' => false
            ],
            'title_tag' => [
                'type' => 'string',
                'default' => 'span'
            ],
            'title_text_color' => [
                'type' => 'string',
                'default' => false
            ],
            'title_background_color' => [
                'type' => 'string',
                'default' => false
            ],
            'border_color' => [
                'type' => 'string',
                'default' => false
            ],
            'schema' => [
                'type' => 'string',
                'default' => false
            ]
        ]
    ));
});
