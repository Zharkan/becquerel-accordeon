// Init function
window.onload = function () {
  // Close all details tags automatically
  document.querySelectorAll("details").forEach((D, _, A) => {
    D.ontoggle = (_) => {
      if (D.open)
        A.forEach((d) => {
          if (d != D) d.open = false;
        });
    };
  });

  var allDetails = document.querySelectorAll("details");
  // Now detect changes
  allDetails.forEach((deet) => {
    deet.addEventListener("toggle", toggleOpenOneOnly);
  });

  function toggleOpenOneOnly(e) {
    if (this.open) {
      //Change font weight upon opening tag
      this.children[0].style.fontWeight = "700";
    } else {
      // Revert back upon closing tag
      this.children[0].style.fontWeight = "normal";
    }
  }
};
