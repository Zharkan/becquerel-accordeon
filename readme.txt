=== Becquerel Accordéon ===
Contributors: Gaylord Julien
Donate link: https://www.gaylordjulien.dev
Requires at least: 5.0
Tested up to: 5.9
Requires PHP: 7.0
Stable tag: 1.5.7
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl.html

Block accordéon pour Becquerel.